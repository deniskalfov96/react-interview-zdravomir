import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from './Home';
import Login from './components/Login';
import Search from './components/Search';
import Logout from './components/Logout';

export default function App() {
    return (
        <div>
            <Switch>
                <Route path="/login" component={Login} />
                <Route path="/logout" component={Logout} />
                <Route path="/search" component={Search} />
                <Route path="/" component={Home} />
            </Switch>
        </div>
    );
}
