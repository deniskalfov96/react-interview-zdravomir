
import React from "react";
import { makeLogout } from '../actions/logout.action';
import { connect } from "react-redux";

const Logout = function (props) {

    props.makeLogout();
    props.history.push('/login');

    return (
        <h2>Logout</h2>
    );
}

const mapStateToProps = (state, ownProps) => {
    return {
    }
}

export default connect(mapStateToProps, { makeLogout })(Logout)
