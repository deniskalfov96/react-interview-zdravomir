import React, { useState } from "react";
import { useQuery } from "react-query";
import { API_URL } from "../constants";
import { connect, useStore } from "react-redux";
import { setSearchValue, setSearchValueList } from '../actions/search.action'
import TextField from '@material-ui/core/TextField';
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import CreatableSelect from 'react-select/creatable';

const filter = createFilterOptions();

const Search = function (props) {
    const state = useStore().getState();
    const access_token = state.login && state.login.access_token ? state.login.access_token : null;

    const searchVal = props.search && props.search.value ? props.search.value : '';
    const searchValSelected = props.search && props.search.value_list ? props.search.value_list : '';

    const queryDetailsSearchAutocomplete = useQuery("search", () =>
        fetch(API_URL + `api/search?limit=${searchVal ? 10 : 20}&offset=0&type=posts&search=` + (searchVal || searchValSelected), {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + access_token,
            }
        }).then((res) => res.json())
        , { enabled: false }
    );

    const { isLoading, error, data } = queryDetailsSearchAutocomplete;

    let postTitles = [];
    if (data && data.posts) {
        data.posts.map(p => {
            let label = <>{p.title}</>;
            if (p.user && p.user.avatar_url) {
                label = <div><img src={p.user.avatar_url} style={{ maxWidth: 75, maxHeight: 75 }} /> - {p.title}</div>
            }

            postTitles.push({
                value: p.title,
                label: label,
                // image: p.user && p.user.avatar_url
            })
        });
    }

    if (!access_token) {
        return 'Not logged in';
    }

    if (isLoading) return "Loading...";

    if (error) return "An error has occurred: " + error.message;

    return (
        <div>
            {/* <div>{isFetching ? "Updating..." : ""}</div> */}

            {/* <Autocomplete
                value={(props.search && props.search.value && props.search.value.title ? props.search.value.title : '')}
                onChange={(event, newValue) => {
                    if (typeof newValue === 'string') {
                        console.log(`newValue1 `, newValue)
                        props.setSearchValue(
                            newValue,
                        );
                    } else if (newValue && newValue.inputValue) {
                        console.log(`newValue2 `, newValue)
                        // Create a new value from the user input
                        props.setSearchValue(
                            newValue.inputValue,
                        );
                        // queryDetailsSearchAutocomplete.refetch();
                    } else {
                        console.log(`newValue3 `, newValue)
                        props.setSearchValue(newValue);
                        // queryDetailsSearchAutocomplete.refetch();
                    }
                }}
                filterOptions={(options, params) => {
                    const filtered = filter(options, params);

                    // Suggest the creation of a new value
                    if (params.inputValue !== '') {
                        console.log(`params.inputValue`, params.inputValue)
                        queryDetailsSearchAutocomplete.refetch();
                        props.setSearchValue(params.inputValue);

                        filtered.push({
                            inputValue: params.inputValue,
                            title: `Add "${params.inputValue}"`,
                        });
                    }

                    return filtered;
                }}
                selectOnFocus
                clearOnBlur
                handleHomeEndKeys
                id="free-solo-with-text-demo"
                options={postTitles}
                getOptionLabel={(option) => {
                    // Value selected with enter, right from the input
                    if (typeof option === 'string') {
                        return option;
                    }
                    // Add "xxx" option created dynamically
                    if (option.inputValue) {
                        return option.inputValue;
                    }
                    // Regular option
                    return option.title;
                }}
                renderOption={(option) => (
                    <>
                        {option.image ? <img src={option.image} style={{ maxWidth: 75, maxHeight: 75 }} /> : null}
                        {option.title}
                    </>
                )}
                style={{ width: 500 }}
                freeSolo
                renderInput={(params) => (
                    <TextField {...params}
                        label="Search posts"
                        variant="outlined"
                    />
                )}
            /> */}


            <CreatableSelect
                isClearable
                onChange={(e) => {
                    props.setSearchValueList(e && e.value ? e.value : '')
                    queryDetailsSearchAutocomplete.refetch();
                }}
                onInputChange={(e) => {
                    props.setSearchValue(e || '')
                    queryDetailsSearchAutocomplete.refetch();
                }}
                options={postTitles}
            />

            {props.search && props.search.value_list && data && data.posts ?
                <>
                    {data.posts.map(p =>
                        <div>
                            <p>{p.title}</p>
                            <p>{p.user && p.user.name ? p.user.name : ''}</p>
                            <p>{p.user && p.user.avatar_url ? <img src={p.user.avatar_url} style={{ maxWidth: 75, maxHeight: 75 }} /> : ''}</p>
                            <hr />
                        </div>
                    )}
                </>
                :
                null
            }

        </div>
    )
}

const mapStateToProps = (state, ownProps) => {
    return {
        search: state.search || {},
    }
}

export default connect(mapStateToProps, { setSearchValue, setSearchValueList })(Search)

