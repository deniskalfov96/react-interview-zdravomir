import React, { useState, useEffect } from "react";
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { makeLogin, setLoginError } from '../actions/login.action';
import { connect, useStore, useDispatch } from "react-redux"
import { useQuery } from "react-query";
import { API_URL } from './../constants';

const LoginPage = function (props) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const dispatch = useDispatch();

    const loginReqOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            "email": email,
            "password": password,
        })
    };

    const queryDetails = useQuery('repoData', () =>
        fetch(API_URL + 'api/auth/login', loginReqOptions)
            .then(res => res.json())
        , { enabled: false }
    )

    if (queryDetails.data && queryDetails.data.access_token) {
        dispatch(makeLogin(queryDetails.data.access_token, props.history));
    } else if (queryDetails.data && queryDetails.data.error) {
        dispatch(setLoginError(queryDetails.data.error));
    }

    const login = (e) => {
        e.preventDefault();
        queryDetails.refetch();
    }

    useEffect(() => {
    }, [props])


    return (
        <Form onSubmit={(e) => login(e)}>
            <FormGroup>
                <Label for="email">Email</Label>
                <Input type="email" name="email" id="email" placeholder="Write your email" onChange={(e) => setEmail(e.target.value)} />
            </FormGroup>
            <FormGroup>
                <Label for="password">Password</Label>
                <Input type="password" name="password" id="password" placeholder="Write your password" onChange={(e) => setPassword(e.target.value)} />
            </FormGroup>
            <Button onClick={(e) => login(e)}>Submit</Button>
        </Form >
    );
}

const mapStateToProps = (state, ownProps) => {
    return {
        login: state.login || {},
    }
}

export default connect(mapStateToProps, { makeLogin, setLoginError })(LoginPage)