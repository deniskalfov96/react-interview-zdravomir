import {
    LOGIN_SUCCESS, LOGIN_FAILED, MAKE_LOGOUT
} from '../actions/types';

const initialState = {
    access_token: '',
    login_error: ''
}

export default function (state = initialState, action) {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return {
                ...state,
                access_token: action.payload,
                login_error: '',
            }
        case LOGIN_FAILED:
            return {
                ...state,
                access_token: '',
                login_error: action.payload,
            }
        case MAKE_LOGOUT:
            return {
                ...state,
                access_token: '',
                login_error: '',
            }
        default:
            return state;
    }
}