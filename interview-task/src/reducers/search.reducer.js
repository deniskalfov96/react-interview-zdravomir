import {
    SET_SEARCH_VALUE, SET_SEARCH_VALUE_LIST
} from '../actions/types';

const initialState = {
    value: '',
    value_list: ''
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_SEARCH_VALUE:
            return {
                ...state,
                value: action.payload,
            }
        case SET_SEARCH_VALUE_LIST:
            return {
                ...state,
                value_list: action.payload,
            }
        default:
            return state;
    }
}