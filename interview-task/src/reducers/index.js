import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; //localstorage object - as default storage

import loginReducer from './login.reducer';
import searchReducer from './search.reducer';

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['login', 'search']
}

const rootReducer = combineReducers({
    login: loginReducer,
    search: searchReducer
});

export default persistReducer(persistConfig, rootReducer)