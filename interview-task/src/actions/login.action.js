import axios from 'axios';
import { LOGIN_SUCCESS, LOGIN_FAILED } from './types';
import { API_URL, WEB_APP_URL } from './../constants';

export const makeLogin = (data, history) => dispatch => {
    dispatch({
        type: LOGIN_SUCCESS,
        payload: data
    })
    history.push(WEB_APP_URL + 'search')
}

export const setLoginError = (e) => dispatch => {
    dispatch({
        type: LOGIN_FAILED,
        payload: e
    })
}