import { SET_SEARCH_VALUE, SET_SEARCH_VALUE_LIST } from './types';

export const setSearchValue = (searchValue) => dispatch => {
    dispatch({
        type: SET_SEARCH_VALUE,
        payload: searchValue
    })
}

export const setSearchValueList = (searchValueList) => dispatch => {
    dispatch({
        type: SET_SEARCH_VALUE_LIST,
        payload: searchValueList
    })
}

