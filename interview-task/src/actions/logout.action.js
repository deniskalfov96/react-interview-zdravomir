import { MAKE_LOGOUT } from './types';

export const makeLogout = () => dispatch => {
    dispatch({
        type: MAKE_LOGOUT,
    })
}
