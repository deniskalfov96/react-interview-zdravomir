import React from "react";
import {
    Link,
} from "react-router-dom";
import { Button } from 'reactstrap';

export default function Header() {
    return (
        <div style={{ marginTop: 25, marginBottom: 30, textAlign: 'center' }}>
            <Link to="/login" style={{ margin: 10 }}>
                <Button color="success">Log in</Button>
            </Link>
            <Link to="/search" style={{ margin: 10 }}>
                <Button color="info">Search</Button>
            </Link>
            <Link to="/logout" style={{ margin: 10 }}>
                <Button color="danger">Log out</Button>
            </Link>
        </div>
    );
}