import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Router from "./Router";
import Header from "./Header";

function App() {
  return (
    <>
      <Header />
      <Router />
    </>
  );
}

export default App;
